### Belajar Bahasa Pemograman `python`, `javascript`, `php`,`c++` 


```python
//Menggunakan python
    def Genap(a):
        if(a%2==0):
            return 'Bilangan Genap'
        else:
            return 'Bilangan Ganjil`
```

``` typescript
    //Contoh Fungsi Pada Javascript yang menerapkan ES6
    const Genap=(a)=>{
        if(a%2===0){
            return 'Bilangan Genap'
        }else{
            return 'Bilangan Ganjil'
        }
    }
    const Fibonaci=(a)=>{
        if(a===1){
            return 1
        }else{
            return a*Fibonaci(a-1)
        }
    }
```

``` typescript
    //Contoh Lain
    const Tambah(...list){
        let jumlah=0
        for(item of list){
            jumlah+=item
        }
        return jumlah
    }
    //Penggunaannya
    console.log(Tambah(1,4,23,5,4,3))
```

``` php
//Menggunakan PHP
    function Prima(integer $a):void{
        $m=0;
        for($i=1;$<=a;$i++){
            $m+=1;
        }
        if(m==2){
            return 'Bilangan Prima'.PHP_EOL;
        }else{
            return 'Bilangan Kompost'.PHP_EOL;
        }
    }
    //Penggunaan
    echo Prima(10);
```

``` cpp
//Menggunakan C++
include<iostream>
using namespace std;
void Genap(int a){
        int m=0;
        for(i=1;i<=a;i++){
            m+=1
        }
        if(m==2){
            return 'Bilangan Genap'
        }else{
            return 'Bilangan Ganjil'
        }
    }
int main(){
    int number;
    cout<<"-----Contoh Fungsi Genap-----"<<endl;
    cout<<"Masukan Angka ";cin>>number;
    cout<<Genap(number)<<endl;
}

```

### Sedikit Kata2 Tidak Bijak
**'Tidurlah Sebelum Ngantuk'**
